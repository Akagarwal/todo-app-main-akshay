const theme = document.getElementById('theme');
const newItemInput = document.getElementById('additem');
const todoList = document.querySelector('.content ul');
const itemsLeft = document.querySelector('.items-left span');
const getClear = document.querySelector('.clear');

itemsLeft.innerText = document.querySelectorAll('.list-items input[type="checkbox"]').length;

// Theme changer

theme.addEventListener('click', () => {
    document.querySelector('body').classList = [theme.checked ? 'theme-light' : 'theme-dark'];
})

//Createing new TODO

newItemInput.addEventListener('keypress', (e) => {
    if (e.key == 'Enter' && newItemInput.value.length > 0) {
        createNewTodoItem(newItemInput.value)
        newItemInput.value = '';
    }
});

function createNewTodoItem(text) {
    const elem = document.createElement('li');
    elem.classList.add('flex-row');

    elem.innerHTML = `
    <label class="list-items">
        <input type="checkbox">
        <span class="checkmark"></span>
        <span class="text">${text}</span>
    </label>
    <span class="remove"></span>
    `;
    todoList.append(elem);
    updateItemsCount(1);
};

function updateItemsCount(number) {
    itemsLeft.innerText = +itemsLeft.innerText + number
}

// Removing todo items

function removeTodoItem(elem) {
    elem.remove();
    updateItemsCount(-1);
}

todoList.addEventListener('click', (event) => {
    if (event.target.classList.contains('remove'))
        removeTodoItem(event.target.parentElement);
});

// Clear completed

getClear.addEventListener('click', () => {
    document.querySelectorAll('.list-items input[type="checkbox"]:checked').forEach(item => {
        removeTodoItem(item.closest('li'));
    });
});

// Filter todo items

document.querySelectorAll('.filter input').forEach((radio) => {
    radio.addEventListener('change', (e) => {
        filterTodoItems(e.target.id);
    });
});

function filterTodoItems(id) {
    const allItems = todoList.querySelectorAll('li')

    switch (id) {
        case 'all':
            allItems.forEach(item => {
                item.classList.remove('hidden');
            })
            break;
        case 'active':
            allItems.forEach(item => {
                item.querySelector('input').checked ? item.classList.add('hidden') : item.classList.remove('hidden');
            })
            break;
        case 'completed':
            allItems.forEach(item => {
                item.querySelector('input').checked ? item.classList.remove('hidden') : item.classList.add('hidden');
            })
            break;
    }
}